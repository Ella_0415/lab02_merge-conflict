//Graciella Toulassi 2142371
package application;
import vehicles.Bicycle;

public class BikeStore 
{
    public static void main(String[] args)
    {
        Bicycle[] bicArray = new Bicycle[4]; 
        //initializing the bic objects in array
        bicArray[0] = new Bicycle("BMW", 5, 40.0);
        bicArray[1] = new Bicycle("Honda", 5, 45.0);
        bicArray[2] = new Bicycle("Tesla", 7, 55.0);
        bicArray[3] = new Bicycle("Toyota", 6, 60.0);
        //loop to print objects
        for(int i=0; i<bicArray.length; i++)
        {
            System.out.println(bicArray[i]);
        }
    }
}
